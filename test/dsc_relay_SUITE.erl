%%% dsc_relay_SUITE.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015 - 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  @doc Test suite for relay funcationality  of the {@link //dsc. dsc}
%%%  application.
%%%
-module(dsc_relay_SUITE).
-copyright('Copyright (c) 2015 - 2018 SigScale Global Inc.').

%% common_test required callbacks
-export([suite/0, sequences/0, all/0]).
-export([init_per_suite/1, end_per_suite/1]).
-export([init_per_testcase/2, end_per_testcase/2]).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include("dsc.hrl").
-include_lib("common_test/include/ct.hrl").
-include_lib("diameter/include/diameter.hrl").
-include_lib("diameter/include/diameter_gen_base_rfc6733.hrl").
-include_lib("diameter/include/diameter_gen_base_accounting.hrl").

-define(ORIG_PEER_SVC, originating_peer_service).
-define(ORIG_PEER_ALIAS, orignating_peer).
-define(DEST_PEER_SVC, destination_peer_service).
-define(DEST_PEER_ALIAS, destination_peer).
-define(BASE_APPLICATION_ID, 0).
-define(BASE_ACCOUNTING_ID, 3).
-define(RELAY_APPLICATION_ID, 16#FFFFFFFF).
-define(EPOCH_OFFSET, 2208988800).

%%---------------------------------------------------------------------
%%  Test server callback functions
%%---------------------------------------------------------------------

-spec suite() -> DefaultData :: [tuple()].
%% Require variables and set default values for the suite.
%%
suite() ->
	[{userdata, [{doc, "Test suite for relay functionality in DSC"}]},
	{timetrap, {seconds, 60}}].

-spec init_per_suite(Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before the whole suite.
%%
init_per_suite(Config) ->
	Config.

-spec end_per_suite(Config :: [tuple()]) -> any().
%% Cleanup after the whole suite.
%%
end_per_suite(Config) ->
	Config.

-spec init_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before each test case.
%%
init_per_testcase(_TestCase, Config) ->
	case is_alive() of
		true ->
			RelayAddress = {0,0,0,0},
			RelayPort = dsc_test_lib:port(),
			OrigPeerRealm = "alice.com",
			DestPeerRealm = "bob.com",
			{ok, Host} = inet:gethostname(),
			[{address, RelayAddress}, {port, RelayPort}, {orig_peer_realm, OrigPeerRealm},
					{dest_peer_realm, DestPeerRealm}, {host, Host} | Config];
		false ->
			{skip, node_not_alive}
	end.

-spec end_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> any().
%% Cleanup after each test case.
%%
end_per_testcase(_TestCase, _Config) ->
		ok.

-spec sequences() -> Sequences :: [{SeqName :: atom(), Testcases :: [atom()]}].
%% Group test cases into a test sequence.
%%
sequences() ->
	[].

-spec all() -> TestCases :: [Case :: atom()].
%% Returns a list of all test cases in this test suite.
%%
all() ->
	[relay_echo, relay_load].

%%---------------------------------------------------------------------
%%  Test cases
%%---------------------------------------------------------------------

relay_echo() ->
	[{userdata, [{doc, "relay_echo"}]}].

relay_echo(Config) ->
	OrigPeerRealm  = ?config(orig_peer_realm, Config),
	DestPeerRealm  = ?config(dest_peer_realm, Config),
	RelayAddress = ?config(address, Config),
	RelayPort = ?config(port, Config),
	Host = ?config(host, Config),
	{Dsc, OrigPeerTrans, DestPeerTrans} = setup_env(Config, RelayAddress, RelayPort, OrigPeerRealm, DestPeerRealm),
	Ref = erlang:ref_to_list(make_ref()),
	SId = diameter:session_id(Ref),
	ACR = #diameter_base_accounting_ACR{'Session-Id' = SId,
			'Origin-Host' = Host, 'Origin-Realm' = OrigPeerRealm,
			'Destination-Realm' = DestPeerRealm,
			'Accounting-Record-Type' = 1,
			'Accounting-Record-Number' = rand:uniform(10)},
	{ok, ACA} = diameter:call(?ORIG_PEER_SVC, ?ORIG_PEER_ALIAS, ACR, []),
	DestHost = "nas." ++ DestPeerRealm,
	DestHostB = list_to_binary(DestHost),
	DestPeerRealmB = list_to_binary(DestPeerRealm),
	SIdB = list_to_binary(SId),
	#diameter_base_accounting_ACA{'Session-Id' = SIdB,
			'Result-Code' = ?'DIAMETER_BASE_RESULT-CODE_SUCCESS',
			'Origin-Host' = DestHostB, 'Origin-Realm' = DestPeerRealmB} = ACA,
	ok = stop_env(Dsc, OrigPeerTrans, DestPeerTrans).

relay_load() ->
	[{userdata, [{doc, "Send multiple requests to DSC"}]}].

relay_load(Config) ->
	OrigPeerRealm  = ?config(orig_peer_realm, Config),
	DestPeerRealm  = ?config(dest_peer_realm, Config),
	RelayAddress = ?config(address, Config),
	RelayPort = ?config(port, Config),
	Host = ?config(host, Config),
	{Dsc, OrigPeerTrans, DestPeerTrans} = setup_env(Config, RelayAddress, RelayPort, OrigPeerRealm, DestPeerRealm),
	F1 = fun() ->
				Ref = erlang:ref_to_list(make_ref()),
				SId = diameter:session_id(Ref),
				ACR = #diameter_base_accounting_ACR{'Session-Id' = SId,
						'Origin-Host' = Host, 'Origin-Realm' = OrigPeerRealm,
						'Destination-Realm' = DestPeerRealm,
						'Accounting-Record-Type' = 1,
						'Accounting-Record-Number' = rand:uniform(10)},
				{ok, _R} = diameter:call(?ORIG_PEER_SVC, ?ORIG_PEER_ALIAS, ACR, [])
	end,
	process_flag(trap_exit, true),
	F2 = fun(_, 0, Acc) ->
				lists:reverse(Acc);
			(F2, N, Acc) ->
				NewAcc = [proc_lib:spawn_link(F1) | Acc],
			F2(F2, N - 1, NewAcc)
	end,
	Pids = F2(F2, 100, []),
	F3 = fun(_, []) ->
				ok;
			(F3, Acc) ->
				receive
					{'EXIT', P, normal} ->
						F3(F3, lists:delete(P, Acc))
				end
	end,
	ok = F3(F3, Pids),
	ok = stop_env(Dsc, OrigPeerTrans, DestPeerTrans).


%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

%% @doc Setup test environment. (Start DSC and multiple DIAMETER peers)
%% @hidden
setup_env(Config, RelayAddress, RelayPort, OrigPeerRealm, DestPeerRealm) ->
	% Start DSC
	{ok, Host} = inet:gethostname(),
	Node = "dsc" ++ integer_to_list(erlang:unique_integer([positive])),
	Path1 = filename:dirname(code:which(dsc_app)),
	PrivDir = ?config(priv_dir, Config),
	ErlFlags = "-mnesia_dir " ++ PrivDir ++ " -pa " ++ Path1,
	{ok, DscNode} = slave:start_link(Host, Node, ErlFlags),
	{ok, _} = rpc:call(DscNode, dsc_app, install, [[DscNode]]),
	ok = rpc:call(DscNode, application, start, [diameter]),
	ok = rpc:call(DscNode, application, set_env, [dsc, traffic_log_dir, PrivDir, [{persistent, true}]]),
	ok = rpc:call(DscNode, application, start, [dsc]),
	OrigPeerRealm = "alice.com",
	DestPeerRealm = "bob.com",
	DestPeerHost = "nas." ++ DestPeerRealm,
	{ok, _} = rpc:call(DscNode, dsc, add_realm, [DestPeerRealm, 0, relay, [DestPeerHost]]),
	T1 = transport1,
	RelayTOpts = [{capabilities,
			[{'Origin-Host', "foo.example.org"},
			{'Origin-Realm', "example.org"},
			{'Product-Name', "SigScale DSC"},
			{'Vendor-Id', 50386}]},
		{transport_module, diameter_sctp},
		{transport_config,
				[{reuseaddr, true},
				 {ip, RelayAddress},
				 {port, RelayPort}]}],
	{ok, _} = rpc:call(DscNode, dsc, add_transport, [T1, listen, RelayTOpts]),
	{ok, _Ref} = rpc:call(DscNode, dsc, start_transport, [T1]),
	receive after 2000 -> ok end,
	ok = application:start(diameter),
	% Start Originating Peer
	{ok, OrigTRef} = start_peer(?ORIG_PEER_SVC, ?ORIG_PEER_ALIAS, OrigPeerRealm, RelayAddress, RelayPort),
	% Start Destination Peer
	{ok, DestTRef} = start_peer(?DEST_PEER_SVC, ?DEST_PEER_ALIAS, DestPeerRealm, RelayAddress, RelayPort),
	{DscNode, OrigTRef, DestTRef}.

%% @doc Start a DIAMETER peer and Connect to DSC at `RAddress' and `RPort'
%% @hidden
start_peer(SvcName, Alias, Realm, RAddress, RPort)  ->
	start_peer(SvcName, Alias, Realm, [{connect_timer, 30000} | transport_opts(RAddress, RPort, diameter_sctp)]).

%% @hidden
start_peer(SvcName, Alias, Realm, Opts)->
	true = diameter:subscribe(SvcName),
	ok = diameter:start_service(SvcName, service_opts(Realm, Alias, [])),
	true = diameter:subscribe(SvcName),
	{ok, TRef} = diameter:add_transport(SvcName, {connect, Opts}),
	receive
		#diameter_event{service = SvcName, info = start} ->
			receive
					#diameter_event{service = SvcName, info = {up, _, _, _}} ->
						ok;
					#diameter_event{service = SvcName, info = {up, _, _, _, _}} ->
						receive
							#diameter_event{service = SvcName, info = {watchdog, _, _, _, _}} ->
								{ok, TRef};
							_ ->
								{error, watchdog_not_received}
						end;
					_E ->
						{error, transport_not_ready}
				end;
		_ ->
			{skip, peer_not_started}
	end.

%% @doc Stop test environment. (Stop DSC and multiple DIAMETER peers)
%% @hidden
stop_env(DscNode, OrigTRef, DestTRef) ->
	% Stop DSC
	ok = rpc:call(DscNode, application, stop, [dsc]),
	ok = rpc:call(DscNode, application, stop, [diameter]),
	ok = slave:stop(DscNode),
	% Stop peers
	ok = stop_peer(?ORIG_PEER_SVC, OrigTRef),
	ok = stop_peer(?DEST_PEER_SVC, DestTRef),
	application:stop(diameter).


%% @hidden
stop_peer(Svc, TransRef) ->
	ok = diameter:remove_transport(Svc, TransRef),
	ok = diameter:stop_service(Svc).

%% @hidden
service_opts(Realm, Alias, Options) ->
	Options ++ [{'Origin-Host', "nas." ++ Realm},
		{'Origin-Realm', Realm},
		{'Vendor-Id', 13019},
		{'Product-Name', "SigScale Test Client"},
		{'Auth-Application-Id', [?BASE_ACCOUNTING_ID]},
		{string_decode, false},
		{restrict_connections, false},
		{application, [{alias, Alias},
				{dictionary, diameter_gen_base_accounting},
				{module, diameter_test_client_cb}]},
		{application, [{alias, base_alias},
				{dictionary, diameter_gen_base_rfc6733},
				{module, diameter_test_client_cb}]}].

%% @hidden
transport_opts(Address, Port, Trans) when is_atom(Trans) ->
	transport_opts1({Trans, Address, Address, Port}).

%% @hidden
transport_opts1({Trans, LocalAddr, RemAddr, RemPort}) ->
	[{transport_module, Trans},
		{transport_config, [{raddr, addr(RemAddr)},
		{rport, RemPort},
		{reuseaddr, true}
		| [{ip, LocalAddr}]]}].

%% @hidden
addr({0,0,0,0}) ->
	{127,0,0,1};
addr(A) ->
	A.

