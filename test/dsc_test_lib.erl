
%%% vim: ts=3
%%%
-module(dsc_test_lib).

-export([initialize_db/1, start/0, stop/0]).
-export([ipv4/0, port/0]).
-export([random_str/0]).
-export([peer_active/0, peer_transport/0]).
-export([realm_mode/0]).

%% support deprecated_time_unit()
-define(MILLISECOND, milli_seconds).
%-define(MILLISECOND, millisecond).

-include("dsc.hrl").

initialize_db(Dir) ->
	ok = application:set_env(mnesia, dir, Dir),
	case mnesia:system_info(is_running) of
		no ->
			ok = application:start(mnesia),
			initialize_db(Dir);
		S when S == starting; S == stopping ->
			receive
				after 1000 ->
					initialize_db(Dir)
			end;
		yes ->
			case mnesia:wait_for_tables([dsc_transport,  dsc_realm], 1000) of
				{timeout, _} ->
					ok = application:stop(mnesia),
					{ok, Tables} = dsc_app:install(),
					F = fun(T) ->
						case T of
							T when T == dsc_transport; T == dsc_realm ->
								true;
							_ ->
								false
						end
					end,
					lists:all(F, Tables),
					initialize_db(Dir);
				ok ->
					ok
			end
	end.

start() ->
	start([diameter, dsc]).

start([H | T]) ->
	case application:start(H) of
		ok  ->
			start(T);
		{error, {already_started, H}} ->
			start(T);
		{error, Reason} ->
			{error, Reason}
	end;
start([]) ->
	ok.

stop() ->
	case application:stop(dsc) of
		ok ->
			case application:stop(diameter) of
				ok ->
					case application:stop(mnesia) of
						ok ->
							ok;
						{error, Reason} ->
							{error, Reason}
					end;
				{error, Reason} ->
					{error, Reason}
			end;
		{error, Reason} ->
			{error, Reason}
	end.

ipv4() ->
	{10, rand:uniform(256) - 1, rand:uniform(256) - 1, rand:uniform(254)}.

port() ->
	rand:uniform(66559) + 1024.

random_str() ->
	Chars = lists:seq($a, $z) ++ lists:seq($A, $Z) ++ lists:seq($0, $9) ++ [$-], 
	Size = length(Chars),
	random1([], Chars, Size, rand:uniform(6)).

random1([$-], Chars, Size, N) ->
	NewAcc = [lists:nth(rand:uniform(Size), Chars)],
	random1(NewAcc, Chars, Size, N);
random1([$- | T], Chars, Size, _N) ->
	NewAcc = [lists:nth(rand:uniform(Size), Chars) | T],
	random1(NewAcc, Chars, Size, 0);
random1(Acc, _, _, 0) ->
	lists:reverse(Acc);
random1(Acc, Chars, Size, N) ->
	NewAcc = [lists:nth(rand:uniform(Size), Chars) | Acc],
	random1(NewAcc, Chars, Size, N - 1).

peer_active() ->
	State = [true, false],
	N = rand:uniform(length(State)),
	lists:nth(N, State).

peer_transport() ->
	Transport = [tcp, sctp, tls_tcp, dtls_sctp],
	N = rand:uniform(length(Transport)),
	lists:nth(N, Transport).

realm_mode() ->
	Transport = [local, relay, proxy, redirect],
	N = rand:uniform(length(Transport)),
	lists:nth(N, Transport).

