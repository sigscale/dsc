%%% dsc_service_fsm.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015 - 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This {@link //stdlib/gen_fsm. gen_fsm} behaviour callback
%%% 	module implements functions to subscribe to a {@link //diameter. diameter}
%%% 	service and to react to events sent by {@link //diameter. diameter} service.
%%%
%%% @reference <a href="https://tools.ietf.org/pdf/rfc6733.pdf">
%%% 	RFC6733 - DIAMETER Base Protocol</a>
%%%
-module(dsc_service_fsm).
-copyright('Copyright (c) 2015 - 2018 SigScale Global Inc.').

-behaviour(gen_fsm).

%% export the dsc_service_fsm API
-export([]).

%% export the dsc_radius_disconnect_fsm state callbacks
-export([wait_for_start/3, started/3, wait_for_stop/3]).

%% export the call backs needed for gen_fsm behaviour
-export([init/1, handle_event/3, handle_sync_event/4, handle_info/3,
			terminate/3, code_change/4]).

-include_lib("diameter/include/diameter.hrl").
-include_lib("kernel/include/inet.hrl").
-include("dsc.hrl").

-record(statedata,
		{options :: undefined | list()}).

-define(BASE_APPLICATION_ID, 0).
-define(BASE_APPLICATION_CALLBACK, dsc_diameter_base_application_cb).
-define(RELAY_APPLICATION_ID, 16#FFFFFFFF).
-define(RELAY_APPLICATION, dsc_diameter_relay_application).

%%----------------------------------------------------------------------
%%  The dsc_service_fsm API
%%----------------------------------------------------------------------

%%----------------------------------------------------------------------
%%  The dsc_service_fsm gen_fsm call backs
%%----------------------------------------------------------------------

-spec init(Args) -> Result
	when
		Args :: list(),
		Result :: {ok, StateName, StateData}
			| {ok, StateName, StateData, Timeout}
			| {ok, StateName, StateData, hibernate}
			| {stop, Reason} | ignore,
		StateName :: atom(),
		StateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: term().
%% @doc Initialize the {@module} finite state machine.
%% @see //stdlib/gen_fsm:init/1
%% @private
%%
init([Options] = _Args) ->
	process_flag(trap_exit, true),
	SOptions = service_options(Options),
	SvcName = ?DSC_SVC,
	diameter:subscribe(SvcName),
	case diameter:start_service(SvcName, SOptions) of
		ok ->
			init1(#statedata{options = Options});
		{error, Reason} ->
			{stop, Reason}
	end.

%% @hidden
init1(StateData) ->
	case dsc_log:traffic_open() of
		ok ->
			process_flag(trap_exit, true),
			{ok, wait_for_start, StateData};
		{error, Reason} ->
			{stop, Reason}
	end.

-spec wait_for_start(Event, From, StateData) -> Result
	when
		Event :: timeout | term(), 
		From :: {pid(), term()},
		StateData :: #statedata{},
		Result :: {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason, NewStateData},
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle events sent with {@link //stdlib/gen_fsm:sync_send_event/2.
%%		gen_fsm:sync_send_event/2} in the <b>wait_for_start</b> state.
%% @@see //stdlib/gen_fsm:StateName/3
%% @private
%%
wait_for_start(_Event, _From, StateData) ->
	{next_state, wait_for_start, StateData}.

-spec started(Event, From, StateData) -> Result
	when
		Event :: timeout | term(), 
		From :: {pid(), term()},
		StateData :: #statedata{},
		Result :: {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason, NewStateData},
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle events sent with {@link //stdlib/gen_fsm:sync_send_event/2.
%%		gen_fsm:sync_send_event/2} in the <b>started</b> state.
%% @@see //stdlib/gen_fsm:StateName/3
%% @private
%%
started(_Event, _From, StateData) ->
	{next_state, started, StateData}.

-spec wait_for_stop(Event, From, StateData) -> Result
	when
		Event :: timeout | term(), 
		From :: {pid(), term()},
		StateData :: #statedata{},
		Result :: {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason, NewStateData},
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle events sent with {@link //stdlib/gen_fsm:sync_send_event/2.
%%		gen_fsm:sync_send_event/2} in the <b>wait_for_stop</b> state.
%% @@see //stdlib/gen_fsm:StateName/3
%% @private
%%
wait_for_stop(_Event, _From, StateData) ->
	{stop, shutdown, StateData}.

-spec handle_event(Event, StateName, StateData) -> Result
	when
		Event :: term(), 
		StateName :: atom(), 
		StateData :: #statedata{},
		Result :: {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason , NewStateData},
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle an event sent with
%% 	{@link //stdlib/gen_fsm:send_all_state_event/2.
%% 	gen_fsm:send_all_state_event/2}.
%% @see //stdlib/gen_fsm:handle_event/3
%% @private
%%
handle_event(_Event, StateName, StateData) ->
	{next_state, StateName, StateData}.

-spec handle_sync_event(Event, From, StateName, StateData) -> Result
	when
		Event :: term(), 
		From :: {Pid :: pid(), Tag :: term()},
		StateName :: atom(), 
		StateData :: #statedata{},
		Result :: {reply, Reply, NextStateName, NewStateData}
			| {reply, Reply, NextStateName, NewStateData, Timeout}
			| {reply, Reply, NextStateName, NewStateData, hibernate}
			| {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason, Reply, NewStateData}
			| {stop, Reason, NewStateData},
		Reply :: term(),
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle an event sent with
%% 	{@link //stdlib/gen_fsm:sync_send_all_state_event/2.
%% 	gen_fsm:sync_send_all_state_event/2,3}.
%% @see //stdlib/gen_fsm:handle_sync_event/4
%% @private
%%
handle_sync_event(_Event, _From, StateName, StateData) ->
	{reply, ok, StateName, StateData}.

-spec handle_info(Info, StateName, StateData) -> Result
	when
		Info :: term(), 
		StateName :: atom(), 
		StateData :: #statedata{},
		Result :: {next_state, NextStateName, NewStateData}
			| {next_state, NextStateName, NewStateData, Timeout}
			| {next_state, NextStateName, NewStateData, hibernate}
			| {stop, Reason, NewStateData},
		NextStateName :: atom(),
		NewStateData :: #statedata{},
		Timeout :: non_neg_integer() | infinity,
		Reason :: normal | term().
%% @doc Handle a received message.
%% @see //stdlib/gen_fsm:handle_info/3
%% @private
%%
handle_info(#diameter_event{info = start}, wait_for_start, StateData) ->
	{next_state, started, StateData};
handle_info(#diameter_event{info = Event, service = Service},
		StateName, StateData) when element(1, Event) == up;
		element(1, Event) == down ->
	{_PeerRef, #diameter_caps{origin_host = {_, Peer}}} = element(3, Event),
	error_logger:info_report(["DIAMETER event",
			{service, Service}, {event, element(1, Event)},
			{peer, binary_to_list(Peer)}]),
	{next_state, StateName, StateData};
handle_info(#diameter_event{info = {watchdog,
		_Ref, _PeerRef, {_From, _To}, _Config}}, StateName, StateData) ->
	{next_state, StateName, StateData};
handle_info(#diameter_event{info = Event, service = Service}, StateName, StateData) ->
	error_logger:info_report(["DIAMETER event",
			{service, Service}, {event, Event}]),
	{next_state, StateName, StateData}.

-spec terminate(Reason, StateName, StateData) -> any()
	when
		Reason :: normal | shutdown | term(), 
		StateName :: atom(),
		StateData :: #statedata{}.
%% @doc Cleanup and exit.
%% @see //stdlib/gen_fsm:terminate/3
%% @private
%%
terminate(_Reason, _StateName, _StateData) ->
	SvcName = ?DSC_SVC,
	diameter:stop_service(SvcName),
	dsc_log:traffic_close().

-spec code_change(OldVsn, StateName, StateData, Extra) -> Result
	when
		OldVsn :: (Vsn :: term() | {down, Vsn :: term()}),
		StateName :: atom(), 
		StateData :: #statedata{}, 
		Extra :: term(),
		Result :: {ok, NextStateName :: atom(), NewStateData :: #statedata{}}.
%% @doc Update internal state data during a release upgrade&#047;downgrade.
%% @see //stdlib/gen_fsm:code_change/4
%% @private
%%
code_change(_OldVsn, StateName, StateData, _Extra) ->
	{ok, StateName, StateData}.

%%----------------------------------------------------------------------
%%  internal functions
%%----------------------------------------------------------------------

-spec service_options(Options) -> Options
	when
		Options :: list().
%% @doc Returns options for a DIAMETER service
%% @hidden
service_options(Options) ->
	{ok, Vsn} = application:get_key(vsn),
	Version = list_to_integer([C || C <- Vsn, C /= $.]),
	{ok, Hostname} = inet:gethostname(),
	Options1 = case lists:keyfind('Origin-Host', 1, Options) of
		{_, _} ->
			Options;
		false ->
			[{'Origin-Host', Hostname} | Options]
	end,
	Options2 = case lists:keyfind('Origin-Realm', 1, Options1) of
		{_, _} ->
			Options1;
		false ->
			{ok, #hostent{h_name = Realm}} = inet:gethostbyname(Hostname),
			[{'Origin-Realm', Realm} | Options1]
	end,
	Options2 ++ [{'Vendor-Id', 50386},
		{'Product-Name', "SigScale DSC"},
		{'Firmware-Revision', Version},
		{'Auth-Application-Id', [?RELAY_APPLICATION_ID]},
		{'Acct-Application-Id', [?RELAY_APPLICATION_ID]},
		{restrict_connections, false},
		{string_decode, false},
		{application, [{dictionary, diameter_gen_base_rfc6733},
				{module, ?BASE_APPLICATION_CALLBACK}]},
		{application, [{alias, ?RELAY_APPLICATION},
				{dictionary, diameter_gen_relay},
				{module, ?BASE_APPLICATION_CALLBACK}]}].

