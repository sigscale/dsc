%%% dsc.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the public API for the
%%% 	{@link //dsc. dsc} application.
%%%

-module(dsc).

%% export the dsc public API
-export([add_transport/3, find_transport/1, delete_transport/1, start_transport/1, stop_transport/1,
		add_realm/4, find_realm/1, delete_realm/1]).

%% export the dsc public API
-export([start_transport/2]).

-include_lib("diameter/include/diameter.hrl").
-include("dsc.hrl").

-spec add_transport(Name, Type, Options) -> Result
	when
		Name :: term(),
		Type :: connect | listen,
		Options :: [diameter:transport_opt()],
		Result :: {ok, diameter:transport_ref()} | {error, Reason},
		Reason :: term().
%% @doc Add a transport entry in `dsc_transport' table.
%% In order for a successsful DIAMETER capability exchange to happen
%% `Origin-Host', `Origin-Realm', `Vendor-Id', `Product-Name' AVP values must be provided.
%% 
%% For both `listen', `connect' types of transports, `transport_module' should be provided.
%% Unless provided it will default to `diameter_tcp'.
%%
%% For `listen' types, values for `ip' and `port' must be provided.
%%
%% For `connect' types, values  for `raddr', `rport', `ip' must be provided.
%%
%% Example for listening transport:
%%
%% dsc:add_transport(connection19, listen, [{capabilities, [{'Origin-Host', "foo.example.org"}, {'Origin-Realm', "example.org"}, {'Product-Name', "SigScale DSC"},
%% {'Vendor-Id', 50386}]}, {transport_module, diameter_sctp}, {transport_config, [{reuseaddr, true}, {ip, {0,0,0,0}}, {port, 3868}]}]). 
%%
%% Example for a connecting transport :
%% 
%% dsc:add_transport(connection24, connect, [{capabilities, [{'Origin-Host', "foo.example.org"}, {'Origin-Realm', "example.org"}, {'Product-Name', "SigScale DSC"},
%% {'Vendor-Id', 50386}]}, {transport_module, diameter_tcp}, {transport_config, [{raddr, {10,140,0,7}},{rport, 3868}, {reuseaddr, true}, {ip, {0,0,0,0}}]}]).
%%
add_transport(Name, Type, Options) when is_list(Options) ->
	case validate_t_options(Type, Options) of
		ok ->
			F = fun() ->
						R = #dsc_transport{name = Name, type = Type, transport_options = Options},
						ok = mnesia:write(R),
						R
			end,
			case mnesia:transaction(F) of
				{atomic, Transport} ->
					{ok, Transport};
				{aborted, Reason} ->
					{error, Reason}
			end;
		{error, Reason} ->
			{error, Reason}
	end.

-spec find_transport(Name) -> Result
	when
		Name :: term(),
		Result :: {ok, #dsc_transport{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a transport by `Name'
%%
find_transport(Name) ->
	F = fun() ->
				mnesia:read(dsc_transport, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_transport{} = Transport]} ->
			{ok, Transport};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_transport(Name) -> Result
	when
		Name :: term(),
		Result :: ok.
%% @doc Remove a transport from `dsc_transport' table.
delete_transport(Name)  ->
	F = fun() ->
		mnesia:delete(dsc_transport, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec add_realm(Name, ApplicationId, Mode, Peers) -> Result
	when
		Name :: string() | binary(),
		ApplicationId :: non_neg_integer(),
		Mode :: local | relay | proxy | redirect,
		Peers :: [PeerName],
		PeerName :: string(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_realm' table.
%%
add_realm(Name, AppId, Mode, Peers) when is_list(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
add_realm(list_to_binary(Name), AppId, Mode, Peers);
add_realm(Name, AppId, Mode, Peers) when is_binary(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
	F = fun() ->
				R = #dsc_realm{name = Name, app_id = AppId, mode = Mode,
						peers = Peers},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Realm} ->
			{ok, Realm};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_realm(Name) -> Result
	when
		Name :: string() | binary(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a realm by name
%%
find_realm(Name) when is_list(Name) ->
	find_realm(list_to_binary(Name));
find_realm(Name) when is_binary(Name) ->
	F = fun() ->
				mnesia:read(dsc_realm, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_realm{} = Realm]} ->
			{ok, Realm};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_realm(Name) -> ok
	when
		Name :: string() | binary().
%% @doc Delete an entry from the `dsc_realm' table.
delete_realm(Name) when is_list(Name) ->
	delete_realm(list_to_binary(Name));
delete_realm(Name) when is_binary(Name) ->
	F = fun() ->
		mnesia:delete(dsc_realm, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec start_transport(Name) -> Result
	when
		Name :: term(),
		Result :: {ok, Ref} | {error, Reason},
		Ref :: diameter:transport_ref(),
		Reason :: term().
%% @doc Get transport properties from `dsc_transport' table for `Name'
%% and start a transport process.
start_transport(Name)  ->
	F = fun() ->
		mnesia:read(dsc_transport, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_transport{type = Type, transport_options = TOpts}]} ->
			start_transport(Type, TOpts);
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec start_transport(Type, Options) -> Result
	when
		Type :: listen | connect,
		Options :: [diameter:transport_opt()],
		Result :: {ok, Ref} | {error, Reason},
		Ref :: diameter:transport_ref(),
		Reason :: term().
%% @doc Start a transport process.
%% @private
start_transport(Type, Options) ->
	case diameter:add_transport(?DSC_SVC, {Type, Options}) of
		{ok, Ref} ->
			{ok, Ref};
		{error, Reason} ->
			{error, Reason}
	end.

-spec stop_transport(Reference) -> Result
	when
		Reference :: diameter:transport_ref(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Stop a transport process.
stop_transport(Reference) ->
	case diameter:remove_transport(?DSC_SVC, Reference) of
		ok ->
			ok;
		{error, Reason} ->
			{error, Reason}
	end.

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

-spec validate_t_options(Type, Options) -> Result
	when
		Type :: connect | listen,
		Options :: [diameter:transport_opt()],
		Result :: ok | {error, term()}.
%% @hidden
%% @doc Validate transport options for diameter service.
validate_t_options(Type, Options) ->
	validate_t_options1(Type, Options).

%% @hidden
validate_t_options1(Type, Options) ->
	case lists:keyfind(capabilities, 1, Options) of
		{_, Caps} ->
			C1 = lists:keyfind('Origin-Host', 1, Caps),
			C2 = lists:keyfind('Origin-Realm', 1, Caps),
			C3 = lists:keyfind('Product-Name', 1, Caps),
			C4 = lists:keyfind('Vendor-Id', 1, Caps),
			case {C1, C2, C3, C4} of
				{{_,_}, {_,_}, {_,_}, {_,_}} ->
					validate_t_options2(Type, Options);
				_ ->
					{error, capabilities_not_configured}
			end;
		false ->
			{error, capabilities_must_be_provided}
	end.
%% @hidden
validate_t_options2(listen, Options) ->
	case lists:keyfind(transport_config, 1, Options) of
		{_, Config} ->
			C1 = lists:keyfind(ip, 1, Config),
			C2 = lists:keyfind(port, 1, Config),
			case {C1, C2} of
				{{_,_}, {_,_}} ->
					ok;
				_ ->
					{error, listen_transport_not_configured}
			end;
		false ->
			{error, listen_transport_option_missing}
	end;
validate_t_options2(connect, Options) ->
	case lists:keyfind(transport_config, 1, Options) of
		{_, Config} ->
			C1 = lists:keyfind(raddr, 1, Config),
			C2 = lists:keyfind(rport, 1, Config),
			C3 = lists:keyfind(ip, 1, Config),
			case {C1, C2, C3} of
				{{_,_}, {_,_}, {_,_}} ->
					ok;
				_ ->
					{error, connect_transport_not_configured}
			end;
		false ->
			{error, connect_transport_option_missing}
	end.

